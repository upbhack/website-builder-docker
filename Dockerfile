FROM ubuntu:22.04

RUN apt-get update
RUN apt-get install -y git wget latexmk texlive-lang-german golang npm imagemagick
ARG YQ_VERSION=v4.31.2
ARG JQ_VERSION=1.6
ARG HUGO_VERSION=0.111.2
ARG SMARTCROP_VERSION=2.0.3
RUN wget -q https://github.com/mikefarah/yq/releases/download/$YQ_VERSION/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq
RUN wget -q https://github.com/stedolan/jq/releases/download/jq-$JQ_VERSION/jq-linux64 -O /usr/bin/jq && chmod +x /usr/bin/jq
            # https://github.com/gohugoio/hugo/releases/download/v0.111.2/hugo_extended_0.111.2_linux-amd64.deb
RUN wget -q https://github.com/gohugoio/hugo/releases/download/v$HUGO_VERSION/hugo_extended_${HUGO_VERSION}_linux-amd64.deb -O hugo.deb && dpkg -i hugo.deb && rm hugo.deb
# RUN go install github.com/muesli/smartcrop/cmd/smartcrop@$SMARTCROP_VERSION
RUN npm install -g smartcrop-cli@$SMARTCROP_VERSION

env PATH "/root/go/bin/:$PATH"
